from PIL import Image, ImageDraw, ImageFont
import math

# simple char list - from most dense to least dense character
chars = "$4W#o- "[::-1]

charArray = list(chars)
interval = len(charArray)/256 # idk, found this - 256 is minimum
font = ImageFont.truetype('C:\\Windows\\Fonts\\lucon.ttf', 15) # monospaced TrueType font
scale = 0.09
charHeight = 18
charWidth = 10

img = Image.open("input.jpg")

# "resize" image (image same size with different scale)
width, height = img.size
img = img.resize((int(scale*width), int(scale*height*(charWidth/charHeight))), Image.NEAREST)
width, height = img.size
pix = img.load()

# new image / output image
o = Image.new('RGB', (charWidth * width, charHeight * height), color = (0, 0, 0))
d = ImageDraw.Draw(o)

# calc which char should be used for output image
def getChar(inputInt):
    return charArray[math.floor(inputInt*interval)]

for i in range(height):
    for j in range(width):
        r, g, b = pix[j, i]
        # black/white equivalent of rgb
        c = int(r/3 + g/3 + b/3)

        # location, text (char), font, color,
        d.text((j*charWidth, i*charHeight), getChar(c), font = font, fill = (r, g, b))
o.save('output.png')
